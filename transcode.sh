#!/bin/bash
#
# Convert MKV after rip from DVD/BD
# Copy all audio and subtitles
#
# Usage:
#
#     transcode input_file crf [progress_file]
#
# Output file will be same as input file with the string "(rec)"
# appended to the base name. Existing output file are skipped (not
# overwriten). Work is done in a temporary file, to avoid invalid
# files in case of abort.
#
# Default progress goes to stdout. Can be redirected to a file.
# Only start and end time are reported, not all ffmpeg output.
# ffmpeg output always goes to stdout.
#
# Refs:
#
# [1] https://www.chrissearle.org/2018/11/27/transcoding-with-mkv-and-ffmpeg/
# [2] https://superuser.com/a/141343/999889
#
# Note 1:
#
# To test a part of video
# This will start at 30s and convert teh next 10s:
#    ffmpeg ... -ss 30 -i input.mkv ... -t 10 output.mkv
#
# Note 2:
#
# Worth keeping an eye on the new AV1 codec.
# Still experimental and required ffmpeg to be compiled
# https://trac.ffmpeg.org/wiki/Encode/AV1
# https://www.makemkv.com/forum/viewtopic.php?f=3&t=224
#
# by raf
# 22.01.2020

# Check number of arguments.
if [[ "$#" -lt 2 ]] ; then
    echo "Illegal number of parameters."
    echo "Usage:"
    echo "  $0  input_file crf [progress_file]"
    exit 1
fi

# Check if CRF is numeric.
if ! [[ $2 =~ ^[0-9]+$ ]] ; then
    echo "Illegal input for CRF, must be numeric."
    exit 2
fi

# Option to redirect progress output.
logfile=/dev/stdout
if [[ "$#" -eq 3 ]] ; then
    logfile="$3"
fi

# Check existence of input file.
if ! [[ -f $1 ]]; then
    echo "File not found: $1"
    exit 3
else
    fname_ext=$(basename -- "$1")
    dirn=$(dirname -- "$1")
    ext="${fname_ext##*.}"
    fname="${fname_ext%.*}"
    fout="${dirn}//${fname} (rec).${ext}"
    # Output to a temp file
    ftmp="${dirn}//temp_ongoing_file.${ext}"

    echo "$(date) started $fout" >> "$logfile"
    
    if [[ -f $fout ]] ; then
	echo "Output file already exists. Skipping." >> "$logfile"
    else
	# the -map 0 option will select all audio and subtitles tracks
        ffmpeg -hide_banner -stats \
               -i "$1" \
               -map 0 -c copy \
	       -c:v libx265 -crf $2 \
               "$ftmp"
	if [[ "$?" -ne 0 ]] ; then
	    rm "$ftmp"
	    echo "ERROR. ffmpeg exit status not 0" >> "$logfile"
	else
	    mv "$ftmp" "$fout"
	    echo "$(date) done" >> "$logfile"
	fi
    fi
    echo "====================================" >> "$logfile"
fi

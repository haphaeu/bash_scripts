#!/bin/bash

#
# Capture bias frames
#
# Pictures must be taken at shortest exposure as possible with cap on.
#
# They are dependent on ISO.
#
# Raf
# 2018-11-30

num=50  # number of shots
gap=5   # interval between shots

for iso in 100 200 400 800 1600
do
    prefix=$(printf "ISO%04d" $iso)
    gphoto2 --set-config /main/imgsettings/iso=$iso
    gphoto2 -F $num -I $gap --capture-image-and-download --filename=${prefix}_%03n.cr2
done

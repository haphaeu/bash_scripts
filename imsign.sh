############################################################################
#
#   imsign - Image Signature
#
#   Add a signature to images and save them into another directory.
#
#      Use is:
#
#   imsign text size directory
#
#   where:
#
#   text     : signature, use "" when required
#   size     : percentage of the image size
#   directory: directory to save the converted pictures
#   
#  
#                                              Rafael Rossi, 31/05/2012
#
#

if [ "$4" != "" ] || [ "$3" = "" ]; then
   echo Error: 3 arguments expected.
   echo Use: imsign text size directory
   echo Note: - directory must *not* exist.
else
	if ! [ -d $3 ]; then
		mkdir $3 
		shopt -s nullglob
		for f in *.jpg *.JPG; do
			echo $f
			s=$(identify -format %h $f)
			r=$(($s*$2/100))
			convert $f -pointsize $r -draw "gravity southeast fill black text 0,12 '$1' fill white text 2,10 '$1' " $3/$f
		done
	else
		echo Error: directory already exists.
	fi
fi


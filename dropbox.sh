#!/bin/bash
#
# Start dropbox deamon, waits until it is synced, then stop it.
#

dropbox start > /dev/null 2>&1
echo "  Dropbox started."
sleep 10
until [ "$result" == "Up to date" ]; do
    result=$(dropbox status)
    sleep 5
done
echo "  Stopping Dropbox."
dropbox stop > /dev/null 2>&1

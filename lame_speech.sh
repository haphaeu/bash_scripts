#
# recode all mp3 in a folder to VBR 8
# this is intended for speach, some loss of quality
# and very small files
#

if [ "$4" != "" ] || [ "$3" = "" ]; then
   echo Error: 3 arguments expected.
   echo Use: $0 author album directory
   echo Notes: directory must *not* exist.
else
   if ! [ -d $3 ]; then
      mkdir $3 
      shopt -s nullglob
      ct=0
      for f in *.mp3 *.MP3; do
         ct=$(($ct+1))
         lame --add-id3v2 -V 8 -h --ta "$1" --tl "$1" --tn "$ct" --tt "$f" "$f" "$3/$f"
      done
   else
      echo Error: directory already exists.
   fi
fi


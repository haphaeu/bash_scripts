#!/bin/bash
#
# Pra fazer isso funcionar no Mint 19.1:
#
# ### HOST (death-star) ##################
#
# - Instalar o modulo NFS no kernel:
#
#   sudo apt install nfs-kernel-server
#
# - Adicionar o ip do client (ryzen7) no /etc/exports:
#
#   echo /media/raf/data 192.168.10.99(rw,sync,no_root_squash,no_subtree_check) >> /etc/exports
#
# - Exportar a lista:
#
#   sudo exportfs -ra
#
# Apos reboot sera necessario re-iniciar o servico:
#
#    sudo systemctl restart nfs-kernel-server
#
# Et voi-là. Pra verificar se a lista foi exportada corretamente:
#
#   showmount -e
#
# Firewall - se o firewall estiver ativado, tem que abrir a porta
# para o client (ryzen7):
#
#  raf@death-star$ sudo ufw allow from 192.168.10.99 to any port nfs
#
# ### CLIENT (ryzen7) ##################
#
# Depois, no cliente:
#
# - Instalar nfs-common
#
#   sudo apt install nfs-common
#
# - Verificar se a lista foi exportada pelo servidor:
#
#   showmount -e 192.168.10.90
#
# Só isso... agora é só:
#
# LEMBRAR DE MONTAR O DRIVE DATA NO DEATH-STAR

sudo mount 192.168.10.90:/media/raf/DATA_HARDDISK   /mnt/death-star

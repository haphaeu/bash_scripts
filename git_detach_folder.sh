#!/bin/bash

# Dettach a folder from a git repo into a second repo, keep history.
#
# Usage:
#     detach_repo <src_repo_root> <src_repo_subdir> <dst_repo> <dst_git_url>
#
# Where:
#     src_repo_root:
#         *full* path to the root of the source repository
#
#     src_repo_subdir:
#         path to the sub-directory to be detached, relative to src_repo_root
#
#     dst_repo:
#         path to the new destination repository to be created.
#
#     dst_git_url
#         URL of the remote git repository. Remote git repo must exist.
#
# https://stackoverflow.com/questions/359424/detach-subdirectory-into-separate-git-repository/17864475#17864475
#


##############################
### Parsing inputs argument ##

# Check number of input arguments
if [ "$4" == "" ]; then
    echo Error in number of arguments.
    echo Usage:
    echo "    " "$(basename -- "$0")" "<src_repo_root> <src_repo_subdir> <dst_repo> <dst_url>"
    exit 1
fi

# Source repository and its sub-directory to be dettached:
src_repo_root=$1
src_repo_subdir=$2
src_repo_subdir_full_path="$src_repo_root/$src_repo_subdir"

if [ ! -d "$src_repo_subdir_full_path" ]; then
    echo Error. Directory $src_repo_subdir_full_path does not exist.
    exit 2
fi

# Destination repository path and its remote URL
dst_repo=$3
dst_git_url=$4

### end of inputs ##
####################

# name of a temporaty branch for subtree to use
branch=__tmp__

pushd $src_repo_root
git subtree split -P $src_repo_subdir -b $branch
popd

mkdir $dst_repo
pushd $dst_repo

git init
git pull $src_repo_root $branch
if [ $? -ne 0 ]; then
    echo Error. Local repository not created.
    exit 3
fi
echo Local repository created.

git remote add origin $dst_git_url
git push -u origin master
if [ $? -ne 0 ]; then
    echo Warning: pushing into remote repository did not go as planned.
    echo Warning: check URL of the remote origin in the new repo.
fi

popd
pushd $src_repo_root
git rm -rf $src_repo_subdir
git branch -D $branch

echo Done.
echo Check if the directory $src_repo_subdir have been completely removed.
echo You should also commit and push changes from source repo.

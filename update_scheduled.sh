#!/bin/bash
#
# Update system and optionally shutdown.
#
# Shutdown is only run when $user is not logged in.
#
# This script is meant to be run in a cron job
# together with a scheduled power-up set up in RTC
# Wave optin in BIOS.
#
# 1. In BIOS, schedule PC to wake up every day at 01:00
#
# 2. Cron job calls this script as rootat 01:05. It will:
#   2.1. Update system
#   2.2. Shutdown PC if $user is not logged in.
#
# author: raf
# 03-01-2021

logfile=/home/raf/updatelog.txt

timestamp=$(date +%Y.%m.%d\ -\ %H\:%M\:%S)
echo "" >> $logfile
echo "" >> $logfile
echo "==================== $timestamp ====================" >> $logfile
echo "" >> $logfile
echo "" >> $logfile

# TODO: update system
apt update -q >> $logfile
apt upgrade -q -y >> $logfile

# Here checking is $user is logged in
# TODO improve this algo
user=raf
if [[ $(who -q | grep $user) ]]
then
    echo "$user logged in, not shutting down." >> $logfile
else
    echo "Shutting down." >> $logfile
    shutdown now
fi

#!/bin/bash

#
# Convert wma files into mp3.
#
# Work recursively in all sub-directories of a base path.
#
# Multithreaded version.
#
# Raf
# 2018-08-16

#              base path
#           vvvvvvvvvvvvvvv
DIRS=$(find /mnt/nas/Music/ -type d)

# set newline as only list delimiter
IFS=$'\n'


for dir in $DIRS
do
    cd "$dir"

    # Count how many flac files in current dir
    count=`ls -1 *.wma 2>/dev/null | wc -l`

    # Only convert if flac files exist
    if [ $count != 0 ]; then
	echo "$dir"
	echo $count

	# Perform conversion using parallel 8 threads
	# parallel -j 8            : run 8 threads
	# ffmpeg   -y              : overwrites existing files
	#          -n              : does not overwrite
	#          -loglevel quiet : no output
	#          -qscale:a 0     : best quality
        parallel -j 8 ffmpeg -y -loglevel quiet -i {} -qscale:a 4 {.}.mp3 ::: ./*.wma
    fi;
done;


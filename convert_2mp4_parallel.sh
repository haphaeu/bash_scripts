#
# Convert all *.MOV videos to MP4 using H264.
#

for video in *.MOV
do
    echo "##################################################################################"
    echo $video
    ffmpeg -n -hide_banner -loglevel warning -stats \
	   -i "$video" -c:a copy -c:v libx265 "${video%.*}.mp4";
done   


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Blame user on file extension size - SIM file watch dog.

Created on 6 Nov 2019

@author: raf
"""


import os
import argparse
from math import log
try:
    import pwd  # Linux
except:
    pwd = None
    import win32security  # Windows

_large_int = int(9e9)


def extuserstats(root, ext):
    '''Calculate file extension size, recursively, per user.

    All sub-directories are summed up and overall statistics shown. Only directories with
    file count or size above threshold are reported vebosely.

    root:
        root path to start search

    ext:
        file extension to search for. Format '.ext'
    '''

    # This will be a dictionary of usernames with total_size
    # {'user1': 1024, 'user2': 2048, ...}
    filesize = dict()

    # sanity check on input
    ext = ext.lower()
    if ext[0] is not '.':
        ext = '.' + ext

    print('Username\tTotal size')

    for dirpath, dirnames, filenames in os.walk(root):
        for filename in filenames:
            fileext = os.path.splitext(filename)[-1].lower()
            full_fname = os.path.join(dirpath, filename)
            if fileext == ext:
                try:
                    tmp_size = os.stat(full_fname).st_size
                    if pwd:
                        owner = pwd.getpwuid(os.stat(full_fname).st_uid).pw_name
                    else:
                        sd = win32security.GetFileSecurity(
                                full_fname,
                                win32security.OWNER_SECURITY_INFORMATION)
                        owner_sid = sd.GetSecurityDescriptorOwner()
                        owner, domain, tp = win32security.LookupAccountSid(None, owner_sid)
                except FileNotFoundError:
                    pass

                try:
                    filesize[owner] += tmp_size
                except KeyError:
                    filesize[owner] = tmp_size

    sorted_sizes = sorted(filesize.items(), key=lambda kv: kv[1])
    sorted_sizes.reverse()
    for user, total_size in sorted_sizes:
        print(user, '\t', format_size(total_size))


def format_size(size_b):
    ''' 512 -> '512 B'
        10240 -> '10.00 kB'
        ...
    '''

    if size_b == 0:
        return '0 B'

    units = ['B', 'kB', 'MB', 'GB', 'TB']
    order = int(log(size_b, 1024))

    return '%.2f %s' % (size_b / (1024**order), units[order])


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
            description='File extension recursive count and size statistics.')

    parser.add_argument('root',
                        nargs='?',
                        type=str,
                        help='Path to root directory to calculate statistics for.'
                        'Works recursivelly into sub directories. Default to current directory.',
                        default=os.getcwd())

    parser.add_argument('--ext', '-x',
                        type=str,
                        help='Define a file extension to look for. '
                        'Default to ".sim" files.',
                        default='.sim')

    args = parser.parse_args()

    print('extstats - ', parser.description)
    print('Root folder   ', args.root)
    print('File extension', args.ext)
    extuserstats(args.root, args.ext)

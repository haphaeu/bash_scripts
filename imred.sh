############################################################################
#
#   imred - Image Reducer
#
#   Resize and recode all pictures within a directory
#   and save the converted pictures into another directory.
#
#      Use is:
#
#   imred quality size directory
#
#   where:
#
#   quality  : jpeg compression level (from 1 to 100)
#   size     : size of the image (in px, smaller side)
#   directory: directory to save the converted pictures
#   
#      Either quality of size can be set to zero to be unchanged. But
#   at least one of them must be input.
#
#      The directory must *not* exist, it will be created. This is done 
#   to preserve any existing file, and because I'm lazy to do a proper
#   check on that. 
#
#      And, the last but not the least: the image converter from 
#   ImageMagick must be on the system.
#  
#                                              Rafael Rossi, 31/05/2012
#

if [ "$4" != "" ] || [ "$3" = "" ]; then
   echo Error: 3 arguments expected.
   echo Use: imred quality size directory
   echo Notes: - quality OR size can be zero not to change it.
   echo        - directory must *not* exist.
else
	if ! [ -d $3 ]; then
		mkdir $3 
		shopt -s nullglob
		for f in *.jpg *.JPG; do
			echo $f
			if [ "$1" = 0 ]; then
				convert -resize $2 $f $3/$f
			elif [ "$2" = 0 ]; then
				convert -quality $1 $f $3/$f
			else
				convert -quality $1 -resize $2 $f $3/$f
			fi
		done
	else
		echo Error: directory already exists.
	fi
fi


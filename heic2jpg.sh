#!/bin/bash

#
# Converte as fotos do iPhone do formato HEIC pra JPG
#
# Parece ter um bug que alguns arquivos ja sao jpg
# mas tem a extensao HEIC... pra isso o cheque no
# segundo `if`
#
# Rafael, Julho 2022

for f in *.HEIC
do
    echo $f
    heif-convert $f $f.jpg > /dev/null
    # delete if success
    if [ $? -gt 0 ]
    then 
	# check if if image is a jpeg (bug!)
	# https://github.com/strukturag/libheif/issues/655
	# https://github.com/strukturag/libheif/issues/111
	if [[ $(file -b $f) =~ JPEG ]]
	then
	    # just rename it
	    echo "It is a badly-named JPG file. Renaming it..."
	    mv $f $f.jpg
	else
	    echo "!!! ERROR"
	    echo "!!! ERROR Check and convert file manually."
	    echo "!!! ERROR"    
	fi
    fi
done

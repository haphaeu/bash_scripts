#!/bin/bash

for filename in *.mp4
do
    # only process videos not ending with _fhd.mp4
    if [[ $filename != *"_fhd.mp4" ]]
    then
	
	echo "#############################################"
	echo    $filename
	if [[ ! -f ${filename%.*}_fhd.mp4 ]]
	then	
	    # Pass option -t 10 before -i to test first 10s only.
	    ffmpeg -stats \
		   -hide_banner \
		   -loglevel warning \
		   -i $filename \
		   -y \
		   -c:v libx264 \
		   -c:a copy \
		   -vf "fps=30,scale=-1:1080" \
		   ${filename%.*}_fhd.mp4
	else
	    echo "Skipping existing output file..."
	fi
    fi
  
done

#!/bin/bash
#
# Compress and encrypt a directory, excluding some sub-directories.
#
# Creates a full archive per week. Differential archives in between.
#
# 
# Input arguments
src=$1 # source directory
dst=$2 # destination directory
pfx=$3 # prefix for the name of the output file
rcp=$4 # recipient public key
exc=$5 # file with strings to exclude from archive

# tidy up of previous exit status files
if [[ -f $dst/${pfx}_done ]]; then
    rm $dst/${pfx}_done
fi

if [[ -f $dst/${pfx}_fail ]]; then
    rm $dst/${pfx}_fail
fi

# creates a file to flag operataion is ongoing
touch $dst/${pfx}_ongoing
    
ext=.tar.gz.gpg
timestamp=$(date +%Y%m%d_%H%M%S)

# Existing diff archives.
echo "  Existing diff archives:"
files_diff=$(find $dst -type f -name "${pfx}*diff$ext")
for f in $files_diff; do
    echo "    $f"
done
		    
# Existing full archives. Latest first.
echo "  Existing full archives:"
files_full=$(find $dst -type f -name "${pfx}*full$ext" | sort -r)
for f in $files_full; do
    echo "    $f"
done

# Loop through the list and gets the age and name of newest file.
# Need to check existence since blank lines may appear.
# note: date +%j return number of days in the year. New year's bug here =)
age=-1
today=$(date +%j | awk '{x=$0+0;print x}')  # awk to remove leading zero
for file in $files_full; do                 # bash thinks that 060 is base 8
    if [[ -f $file ]]; then
	day_file=$(date -r $file +%j | awk '{x=$0+0;print x}')
	(( age= $today - $day_file ))
	echo "  Found previous $age days old archive: $file"
	break
    fi	
done	

# Test if $exc is undefined/empty, if not, add the option to tar:
[[ -z $exc ]] && arg1="" || arg1="--exclude-from=$exc "
    
# If full archive exists and is less than 7 days old: differential archive
if (( age >= 0 && age < 7 )); then
    echo "  Performing a differential archive."
    fn=${pfx}_${timestamp}_diff$ext
    arg2="--after-date=$file"
    delete_old_files=no
# If full archive does not exist or is older than 7 days: full archive
# Also, mark all pre-existing file to be deleted
else
    echo "  Performing a full archive."
    fn=${pfx}_${timestamp}_full$ext
    arg2=""
    delete_old_files=yes
fi

# ######################################################################
# The Core

# Perform the main action, piping: tar | gzip | gpg
tar $arg1 $arg2 -cf - $src --absolute-names \
    | pv -s $(du $arg1 -sb $src | awk '{print $1}') \
    | gzip \
    | gpg --yes -o $dst/$fn -e -r $rcp
retval=$?  # tar exit status

# save exit status as a file:
rm $dst/${pfx}_ongoing

if [[ $retval -eq 0 ]]; then
    # Delete old files
    if [[ "$delete_old_files" = "yes" ]]; then
	for f in $files_diff; do echo Deleting $f; rm "$f"; done
	for f in $files_full; do echo Deleting $f; rm "$f"; done
    fi
    touch $dst/${pfx}_done
else
    touch $dst/${pfx}_fail
fi

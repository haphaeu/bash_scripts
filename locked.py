# -*- coding: utf-8 -*-
"""

Checks for locked files recursively from a root path.

Created on Mon Oct 28 15:55:52 2019

@author: rarossi
"""
import os
import argparse


def is_locked(root_path, listall=False):
    '''Recursive find locked files in root_path.

    Return a list with the first locked file found.

    Set the flag `listall` to True to find all locked files before returning.

    If no locked files are found, return an empty list.
    '''

    locked_files = []

    for root, dirnames, filenames in os.walk(root_path):
        for filename in filenames:
            full_filename = os.path.join(root, filename)
            try:
                pf = open(full_filename, 'a')
                pf.close()
            except PermissionError:
                if not listall:
                    return [full_filename]
                locked_files.append(full_filename)
            except FileNotFoundError:
                pass

    return locked_files


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
            description='Recursive search for locked files.')

    parser.add_argument('root',
                        nargs='?',
                        type=str,
                        help='Path to root directory to begin search.'
                        'Works recursivelly into sub directories. Default to current directory.',
                        default=os.getcwd())

    parser.add_argument('--list', '-l',
                        action='store_true',
                        help='Search for and list all locked files')

    args = parser.parse_args()

    print('locked - ', parser.description)
    if not args.list:
        print('Return first locked file found.')
    else:
        print('Listing all locked files.')

    locked_files = is_locked(args.root, args.list)

    for file in locked_files:
        print(file)

    if not locked_files:
        print('No locked files.')
    elif args.list:
        print('%d files are locked.' % len(locked_files))

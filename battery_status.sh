#!/bin/bash

# battery status located at
# /sys/class/power_supplt/BAT0

echo Battery capacity status every miniute.

status=$(cat /sys/class/power_supply/BAT0/status)
cap0=$(cat /sys/class/power_supply/BAT0/capacity)
echo [$(date)] $status $cap0%.

time=0

while true
do
    sleep 60
    ((time++))
    status=$(cat /sys/class/power_supply/BAT0/status)
    cap=$(cat /sys/class/power_supply/BAT0/capacity)
    time_remaining=$(($time * $cap / ($cap0 - $cap + 1)))
    echo [$(date)] $status $cap% - $time_remaining minutes remaining.
done

#!/bin/bash

#
# Bulb capture shots for Canon 450D using gphoto2.
#
# Usage:
# ######
#
# bulb  num_shots  exp_time  gap_time
#
#    num_shots: number of shots to take
#
#    exp_time: exposure time
#
#    gap_time: interval between 2 shots
#
# 
# Notes:
# ######
# 
# No sanity checks added to input.
# 
# No checks done if camera is properly set.
# 
# 
# http://www.gphoto.org/doc/remote/
#
# Raf
# 2018-11-28


num=$1
exp=$2
gap=$3

echo "bulb - Bulb capture for Canon 450D using gphoto2."
echo "Number of shots   : $num"
echo "Exposure time     : $exp"
echo "Gap between shots : $gap"


echo "Setting up camera for bulb mode"
gphoto2 --set-config shutterspeed=bulb

echo "Starting capture"
for (( i = 1 ; i <= $num ; i++ ))
do
  echo "  shot $i out of $num"
  gphoto2 \
       --set-config bulb=1 \
       --wait-event="${exp}"s \
       --set-config bulb=0 \
       --wait-event-and-download="${gap}"s \
       --filename="%Y%m%d_%H%M%S.jpg" >> out.txt
  
done

echo "Done."
#
# To use a custom filename, use:
#
#   foo=$(printf "img%04d.jpg" $i)
#
# and then pass the argument:
#
#   --filename="${foo}"
#

#!/bin/bash

#
# Capture time lapse
#
# Adjust camera exposure and 
#
# Raf
# 2021-01-03

num=120  # number of shots
gap=30   # interval between shots
#iso=100

prefix=$(printf "ISO%04d" $iso)
#gphoto2 --set-config /main/imgsettings/iso=$iso
gphoto2 -F $num -I $gap --capture-image-and-download --filename=${prefix}_%03n.jpg

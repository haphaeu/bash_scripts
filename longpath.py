# -*- coding: utf-8 -*-
"""

Searches a root directory for filenames with long full path length.

All entries above an user defined threshold are printed.


Created on Mon Oct 21 11:45:43 2019

@author: rarossi
"""
import os
import sys


def search_long_path(root_path, threshold):
    '''Return the paths of files and directories under root_path longer than threshold.'''

    path_above_threshold = list()

    for root, dirnames, filenames in os.walk(root_path):
        for filename in filenames:
            full_name = os.path.join(root, filename)
            if len(full_name) >= threshold:
                path_above_threshold.append(full_name)

    return path_above_threshold


if __name__ == '__main__':

    try:
        threshold = int(sys.argv[1])
        root_path = sys.argv[2]
    except (ValueError, IndexError):
        print('Error. Usage:\n\t', os.path.basename(sys.argv[0]), 'threshold root_path')
        raise SystemExit

    long_pathnames = search_long_path(root_path, threshold)
    max_length, max_path = -1, -1
    for pathname in long_pathnames:
        length = len(pathname)
        print(length, pathname)
        if length > max_length:
            max_length = length
            max_path = pathname

    print('Longest path name:')
    print(max_length, max_path)

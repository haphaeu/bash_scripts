#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Statistics on number and size of files of a given extension, recursive into sub-directories.

Created on 6 Nove 2019

@author: raf
"""


import os
import argparse
from math import log

_large_int = int(9e9)


def extstats(root, count, size, ext):
    '''Calculate file extension count and size, recursively.

    All sub-directories are summed up and overall statistics shown. Only directories with
    file count or size above threshold are reported vebosely.

    root:
        root path to start search

    count:
        threshold for file count to report directories verboselly

    size:
        threshold for file size to report directories verboselly

    ext:
        list with file extension(s) to search for. Format '.ext'
    '''

    print('%5s\t%9s\t%s' % ('files', 'size', 'path'))


    # sanity check on input
    ext = [x.lower() for x in ext]
    for x in ext:
        if x[0] is not '.':
            x = '.' + x

    totsize = {x: 0 for x in ext}
    numfiles = {x: 0 for x in ext}

    for dirpath, dirnames, filenames in os.walk(root):

        dirsize = {x: 0 for x in ext}
        dirnumfiles = {x: 0 for x in ext}

        for filename in filenames:
            fileext = os.path.splitext(filename)[-1].lower()
            if fileext in ext:
                try:
                    tmp_size = os.stat(os.path.join(dirpath, filename)).st_size
                except FileNotFoundError:
                    pass
                dirsize[fileext] += tmp_size
                dirnumfiles[fileext] += 1

        for x in ext:
            totsize[x] += dirsize[x]
            numfiles[x] += dirnumfiles[x]

        dirnumfiles_all = sum(dirnumfiles.values())
        dirsize_all = sum(dirsize.values())
        if dirnumfiles_all > count or dirsize_all > size:
            print('%5d\t%9s\t%s' % (dirnumfiles_all,
                                    format_size(dirsize_all),
                                    os.path.relpath(dirpath, root)))

    print('Totals for', root)
    print('Ext \t # \t Size')
    for x in ext:      
        print('%s \t %d \t %s' % (x, numfiles[x], format_size(totsize[x])))
  

def format_size(size_b):
    ''' 512 -> '512 B'
        10240 -> '10.00 kB'
        ...
    '''

    if size_b == 0:
        return '0 B'

    units = ['B', 'kB', 'MB', 'GB', 'TB']
    order = int(log(size_b, 1024))

    return '%.2f %s' % (size_b / (1024**order), units[order])


def parse_size(size_string):
    ''' Converts a size string into a number of bytes:
        '1kB' -> 1024

    Supported prefixes are:
        b, B for bytes
        k, kB for kilo bytes
        m, MB for mega bytes
        g, GB for giga bytes
        t, TB for tera bytes

    Prefixes are not case sensitive.
    '''
    size_string = size_string.lower()

    prefixes = ('k', 'm', 'g', 't', 'b')
    multipliers = (1024, 1024**2, 1024**3, 1024**4, 1)

    for prefix, multiplier in zip(prefixes, multipliers):
        position = size_string.find(prefix)
        if position > 0:
            break
    else:  # executed if the for look completes without break
        multiplier = 1
        position = len(size_string)

    return int(multiplier * float(size_string[:position]))


class ActionParseSize(argparse.Action):
    '''This class is used to make argparse to automatically parse the --size argument.

    For example: -s 1kB -> 1024.

    This is done using the parse_size function.

    See below when this class is applied as the 'action' argument of the --size option.
    '''

    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None:
            raise ValueError("nargs not allowed")
        super(ActionParseSize, self).__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, parse_size(values))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
            description='File extension recursive count and size statistics.')

    parser.add_argument('--root', '-r',
                        type=str,
                        help='Path to root directory to calculate statistics for.'
                        'Works recursivelly into sub directories. Default to current directory.',
                        default=os.getcwd())

    parser.add_argument('--ext', '-x',
                        type=str,
                        nargs='+',
                        help='List of file extension(s) to look for. '
                        'Default to ".sim" files.',
                        default='.sim')

    parser.add_argument('--count', '-c',
                        type=int,
                        help='Only directories with counter above this threshold will be printed'
                        'All directories are calculated. Default is zero.',
                        default=_large_int)

    parser.add_argument('--size', '-s',
                        type=str,
                        help='Only directories with size above this threshold will be '
                        'printed. All directories are calculated. Default is zero.',
                        action=ActionParseSize,  # call parse_size for this input
                        default=_large_int)

    args = parser.parse_args()

    print('extstats - ', parser.description)
    print('Count threshold of', '-' if args.count == _large_int else args.count)
    print('Size threshold of', '-' if args.size == _large_int else format_size(args.size))
    print('Extension(s)', end='')
    for ext in args.ext:
        print(ext, end=' ')
    print('')

    extstats(args.root, args.count, args.size, args.ext)

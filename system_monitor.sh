
# CPU temperature
temp=$(sensors | grep Package | awk '{print $4}')

# Task with max CPU usage
# this is not good since top is always at the top =P
cpu=$(top -bin 1 | awk '/^(\s?[0-9])/{print $9, $12; exit}')
ps -eo %cpu,comm --sort %cpu | tail -n 1

# internal ip
ip_int=$(ifconfig wlp1s0 | grep inet | awk '{print $2}')

# RAM used
ram=$(free | grep -i mem | awk '{print $3}')

# SWAP used
swap=$(free | grep -i swap | awk '{print $3}')

ts=$(date +%Y-%m-%d%t%H:%m:%S)

echo -e $ts '\t' $ram '\t' $swap '\t' $cpu '\t' $temp

#!/bin/bash
#
# using NFS
#
# not good since must give ip-specific access in NAS configuration
# and only 1 ip is accepted 
#
## sudo mount 192.168.10.84:/nfs/Public   /mnt/nas/Public     
## sudo mount 192.168.10.84:/nfs/raf      /mnt/nas/raf        
## sudo mount 192.168.10.84:/nfs/marie    /mnt/nas/marie      
## sudo mount 192.168.10.84:/nfs/Fotos    /mnt/nas/Fotos      
## sudo mount 192.168.10.84:/nfs/Music    /mnt/nas/Music      

# UPDATE Asus ZenWifi
# IP addresse changed to 192.168.50.150

#
# using smbclient
#
echo Senha:
read -s PASSWD

sudo PASSWD="$PASSWD" mount -v -t cifs //192.168.50.150/Fotos    /mnt/nas/Fotos    -o username=raf,uid=raf
sudo PASSWD="$PASSWD" mount -v -t cifs //192.168.50.150/Music    /mnt/nas/Music    -o username=raf,uid=raf
sudo PASSWD="$PASSWD" mount -v -t cifs //192.168.50.150/raf      /mnt/nas/raf      -o username=raf,uid=raf
sudo PASSWD="$PASSWD" mount -v -t cifs //192.168.50.150/Public   /mnt/nas/Public   -o username=raf,uid=raf
sudo PASSWD="$PASSWD" mount -v -t cifs //192.168.50.150/marie    /mnt/nas/marie    -o username=raf,uid=raf
sudo PASSWD="$PASSWD" mount -v -t cifs //192.168.50.150/Documents /mnt/nas/Documents    -o username=raf,uid=raf

unset PASSWD

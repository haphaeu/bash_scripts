#!/bin/bash
#
# Archives ~/Documents to NAS_drive/Documents.
# 
# A user "cron" is created in the NAS drive, with access only to the
# target directory.
#
# The password of the user "cron" is encrypted and stored in scripts/senha.gpg.
#
# In order to run this as a cron job, I had to allow the user "raf" to mount
# the NAS drive without sudo, by adding this line to fstab:
#
#   //192.168.10.84/Documents  /mnt/cron_nas_docs  cifs username=cron,gid=raf,rw,user,noauto 0 0
#
# After that, nas can be mounted as "cron" username by doing the command below and entering
# "cron" user password.
#
#   mount /mnt/cron_nas_docs
#
#

# The file cron2.log is created/updated during the archive process in cron.
# So a new archive is needed if the log file is out dated compared to ~/Documents
# Note that the cron job appends its output to cron2.log, so its modification
# time will change with the first `echo` in this script. Therefore,
# **do not `echo` anything before the `if` statement below**
#
if [[ -f /home/raf/cron2.log ]] ; then
    # Compare the modification time of the file cron2.log against all the
    # files in the directory ~/Documents
    files=$(find /home/raf/Documents -type f)
    newer_files=0
    for file in $files; do
	if [[ ( -f $file ) && ( $file -nt /home/raf/cron2.log ) ]] ; then
	    echo "[$(date)] Found file(s) newer than archive."
	    newer_files=1
	    break
	fi	
    done	
    if [[ $newer_files -eq 0 ]] ; then
	echo "[$(date)] Archive is up to date. Nothing to do."
	exit 0
    fi
else
    echo "[$(date)] cron2.log does not exist."
fi

#echo ""
#echo "----------------------------------- $(date) ----------------------------------------"
#echo ""
echo "[$(date)] Proceeding with archive."

# Do the archive job

pass=$(gpg --quiet --decrypt /home/raf/scripts/senha.gpg)
PASSWD="$pass" mount -v /mnt/cron_nas_docs
if [ $? -eq 0 ] ; then
    rsync -az /home/raf/Documents /mnt/cron_nas_docs
fi
PASSWD="$pass" umount /mnt/cron_nas_docs
unset pass
unset PASSWD

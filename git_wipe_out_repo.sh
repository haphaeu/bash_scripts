#!/bin/bash
#
# Wipe out a git repository (local and remote).
#
# Warning: HIGHLY DESTRUCTIVE script. Will wipe out all repository files 
# and their history. The repo will be left blank with no history and no
# way to be recovered from within git.
#
# Usage:
#
#     bash git_wipe_out_repo.sh <repo_url>
#
#

if [ "$1" == "" ]; then
    echo Error. Missing URL of the repo to be wiped out.
    echo Usage:
    echo "    " "$0" "<repository_url>"
    exit 1
fi

# URL of the repo to be wiped out. With or without the .git extension
repo_url=$1

# get the repo base name
# https://stackoverflow.com/a/965072/5069105
# This is equivalent to Python:
#     repo_name_ext = os.basename(repo_url)
#     repo_name = os.splitext(repo_name_ext)[0]
#
repo_name_ext=$(basename -- "$repo_url")
repo_name="${repo_name_ext%.*}"

# Clone repository and cd into it, capturing error codes
# $? is the return code of the last command run
git clone $repo_url
ret_git=$?
cd $repo_name
ret_cd=$?

# Only proceed if not errors found during cloning
if [ $ret_git -ne 0 ] || [ $ret_cd -ne 0 ]; then
    echo Error detected during cloning. Aborting.
    exit 2
fi

# Remove all local files
rm *

# https://stackoverflow.com/a/4122190/5069105
# Remove index and pointer to master
rm .git/index .git/refs/heads/master

# Create and commit a dummy file.
echo "https://dev.azure.com/technipfmc-dev/Subsea-InstallationAnalysis" > README
git add README

# Make a commit and force push into remote server
git commit -m "Changed git server."
git push -f

# Delete local repository directory
cd ..
rm -fR $repo_name
